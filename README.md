# ts-core

[![license](https://img.shields.io/badge/license-MIT%2FApache--2.0-blue")](LICENSE-MIT)
[![docs](https://img.shields.io/badge/docs-typescript-blue.svg)](https://aicacia.gitlab.io/libs/ts-core/)
[![npm (scoped)](https://img.shields.io/npm/v/@aicacia/core)](https://www.npmjs.com/package/@aicacia/core)
[![pipelines](https://gitlab.com/aicacia/libs/ts-core/badges/master/pipeline.svg)](https://gitlab.com/aicacia/libs/ts-core/-/pipelines)

aicacia core utils
